import shutil
import smart_open
from sys import platform


def prepend_line(infile, outfile, line):
	with open(infile, 'r', encoding="utf8") as old:
		with open(outfile, 'w', encoding="utf8") as new:
			new.write(str(line) + "\n")
			shutil.copyfileobj(old, new)

def prepend_slow(infile, outfile, line):
	with open(infile, 'r', encoding="utf8") as fin:
		with open(outfile, 'w', encoding="utf8") as fout:
			fout.write(line + "\n")
			for line in fin:
				fout.write(line)

def get_lines(glove_file_name):
    with smart_open.smart_open(glove_file_name, 'r', encoding="utf8") as f:
        num_lines = sum(1 for line in f)
    with smart_open.smart_open(glove_file_name, 'r', encoding="utf8") as f:
        num_dims = len(f.readline().split()) - 1
    return num_lines, num_dims
	
# Input GloVe File
glove_file = "glove.840B.300d.txt"

num_lines, dims = get_lines(glove_file)

# Output Gensim File
gensim_file = 'model2.bin'
gensim_first_line = "{} {}".format(num_lines, dims)

# Prepend the line
if platform == "linux" or platform == "linux2":
	prepend_line(glove_file, gensim_file, gensim_first_line)
else:
	prepend_slow(glove_file, gensim_file, gensim_first_line)